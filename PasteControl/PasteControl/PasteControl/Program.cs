﻿using System;
using System.Collections.Generic;
using PasteWatch;
namespace PasteControl
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;

            WriteHeader();
            WriteMenu();

            
            // Wait for input
            Console.ReadLine();
        }


        static void WriteMenu()
        {
            const string menuItems = "Start Pastewatcher,About,Exit";

            for (int i = 0; i < menuItems.Split(',').Length; i++)
            {
                Console.WriteLine($"{i + 1}. {menuItems.Split(',')[i]}");
            }


            // Ask for user input
            GetInput();
        }

        private static void GetInput()
        {
            int opt;
            Console.WriteLine();
            Console.Write("Your input: ");
            if (int.TryParse(Console.ReadLine(), out opt))
            {
                switch (opt)
                {
                    case 1:
                        initBot();
                        break;
                    case 2:
                        //Nothing
                        break;
                    case 3:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                WriteError("You gave invalid input, please try again! Make sure you only put in the number.");
                GetInput();
            }


        }

        private static void initBot()
        {
            Console.Write("Initializing bot, please wait...");
            Watcher w = new Watcher(60);
            Console.WriteLine(" [OK]");
            Console.Write("Downloading archive...");
            List<string> urlList = w.GetUrlsFromSource(w.GetSingleArchive());
            Console.WriteLine(" [OK]");

            Console.WriteLine("\r\nThe results: \r\n");
            for (int i = 0; i < urlList.Count ; i++)
            {
                Console.WriteLine($"{i}. {urlList[i]}");
            }
        }



        static void WriteError(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;

            System.Threading.Thread.Sleep(3000);
        }

        static void WriteHeader()
        {
            WriteCenter("## This is an implementation example of the PasteWatch library ##", ConsoleColor.Cyan);
        }

        static void WriteCenter(string text, ConsoleColor consoleColor = ConsoleColor.White)
        {
            Console.SetCursorPosition((Console.WindowWidth - text.Length) / 2, Console.CursorTop);
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(text);
        }
    }
}
