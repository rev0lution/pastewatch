﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasteWatch.Models
{
    public class Rule
    {
        public Mode Mode { get; private set; }
        public Extension Extension { get; set; }
    }

    //Enum for the modes
    public enum Mode
    {
        Contains,
        StartsWith
    }

    //Enum for extensions that are to be ignored
    public enum Extension
    {
        gif,
        jpg,
        png,
        js,
        css,
        html,
        php
    }
}
