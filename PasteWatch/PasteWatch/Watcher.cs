﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using static PasteWatch.Helper;

namespace PasteWatch
{
    public class Watcher
    {
        // Initialize the watcher
        public int RefreshRate { get; set; }
        public string PasteUrl { get; } = "http://www.pastebin.com/";

        //Constructor
        public Watcher(int refreshRate)
        {
            this.RefreshRate = refreshRate;
        }

        //Paste methods
        public string GetSingleArchive()
        {
            string url = $"{PasteUrl}archive";
            Tuple<bool, string> result = Helper.CreateRequest(url);

            if (result.Item1)
            {
                //Request is a success
                return result.Item2;
            }
            return string.Empty;
        }


        //Get pastes from HTML code
        public List<string> GetUrlsFromSource(string source)
        {
            List<string> lstResults = new List<string>();
            string rawContent = string.Empty;
            
            Regex linkRegex = new Regex("\b(href=\"/\\w + \")");
            MatchCollection results = linkRegex.Matches(source);

            //Clear out images, other useless links and replace tag
            for (int i = 0; i < results.Count; i++)
            {
                rawContent = results[i].ToString();
                rawContent = rawContent.Replace("\"", "");
                rawContent = rawContent.Replace("href", "");

               
                lstResults.Add(results[i].ToString());
            }

            return lstResults;
        }

        public bool IsFalsePositive(string link)
        {
            return false;
        }

 

    }
}
