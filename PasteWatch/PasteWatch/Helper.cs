﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace PasteWatch
{
    public class Helper
    {
        public static Tuple<bool, string> CreateRequest(string reqUrl)
        {
            string content = string.Empty;
            try
            {
                WebRequest req = WebRequest.Create(reqUrl);
                WebResponse resp = req.GetResponse();
                if (resp != null)
                {
                    using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                    {
                        content = sr.ReadToEnd();
                    }
                }
                resp.Close(); //Close response
                return Tuple.Create(true, content);
            }
            catch (Exception ex)
            {
                LogError(ex);
                return Tuple.Create(false, string.Empty);
            }
        
        }

        public static void LogError(Exception e, string devMessage = "")
        {
            string filePath = $"{Environment.CurrentDirectory}\\logs\\error.log";


            try
            {
                if (!Directory.Exists($"{Environment.CurrentDirectory}\\logs\\"))
                {
                    Directory.CreateDirectory($"{Environment.CurrentDirectory}\\logs\\");
                }

                if (!File.Exists(filePath))
                {
                    File.Create(filePath);
                }


                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.WriteLine($"Error logged on: {DateTime.Today.ToShortDateString()} @ time {DateTime.Now.ToShortTimeString()}");
                    sw.WriteLine($"Error message: {e.Message}");
                    sw.WriteLine($"Detailed error: \r\n{e.InnerException}\r\n");

                    if (!string.IsNullOrEmpty(devMessage))
                    {
                        sw.WriteLine(devMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                //Perform nothing or endless loop
            }
          
        }

        //Get enum values
        public static class EnumUtil
        {
            public static IEnumerable<T> GetValues<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }
        }
    }
}
